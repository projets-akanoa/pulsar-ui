#![deny(clippy::unwrap_used)]

extern crate core;

use eframe::{Frame, Storage, APP_KEY};
use egui::{Context, TextEdit};
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub mod data;
pub mod pulsar_adapter;
mod toggle;

use crate::pulsar_adapter::PulsarAdapter;
use crossbeam_channel::{bounded, Receiver, Sender};
use data::pulsar_data::{ConsumerKey, PulsarConfig};
use data::ui_data::{Server, Topic};
use data::{Command, Event, MessageSend};
use rand::Rng;
use tracing::{info, warn};

lazy_static! {
    static ref CHANNEL_COMMAND: (Sender<Command>, Receiver<Command>) = {
        let (tx, rx) = bounded(10);
        (tx, rx)
    };
    static ref CHANNEL_EVENT: (Sender<Event>, Receiver<Event>) = {
        let (tx, rx) = bounded(10);
        (tx, rx)
    };
    static ref CHANNEL_KILL: (Sender<()>, Receiver<()>) = {
        let (tx, rx) = bounded(1);
        (tx, rx)
    };
}

#[derive(Default, Deserialize, Serialize)]
pub struct PulsarApp {
    servers: HashMap<usize, Server>,
    current_server: usize,
    last_index: usize,
    to_delete: Option<usize>,
    #[serde(skip)]
    app_id: u8,
}

impl PulsarApp {
    pub fn new(creation_context: &eframe::CreationContext<'_>) -> Self {
        let mut app = match creation_context.storage {
            Some(storage) => eframe::get_value(storage, APP_KEY).unwrap_or_default(),
            None => PulsarApp::default(),
        };

        let mut rng = rand::thread_rng();
        app.app_id = rng.gen();

        tokio::spawn(async move {
            let mut adapter = PulsarAdapter::new();

            adapter.run().await
        });

        app
    }
}

/// Wrapper to send a command
/// Handle error message on sending
fn send_command(command: Command) {
    if let Err(err) = CHANNEL_COMMAND.0.send(command) {
        warn!(?err, "Unable to send message  to Pulsar Adapter")
    }
}

impl eframe::App for PulsarApp {
    // Update the state each frame
    fn update(&mut self, ctx: &Context, frame: &mut Frame) {
        let PulsarApp {
            servers,
            current_server,
            last_index,
            to_delete,
            app_id,
        } = self;

        if let Some(index) = to_delete.take() {
            servers.remove(&index);
        }

        let message_received = CHANNEL_EVENT.1.try_recv();
        if let Ok(event) = message_received {
            if let Some(server) = servers.get_mut(current_server) {
                match server.topics.get_mut(&event.destination.topic_id) {
                    Some(topic) => topic.receive_message = event.data,
                    None => warn!("Unable to found topic"),
                }
            }
        }

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                if ui.button("Quit").clicked() {
                    send_command(Command::Kill);
                    if let Err(err) = CHANNEL_KILL.1.recv() {
                        warn!(?err, "Unable to receive message from Pulsar Adapter")
                    }
                    frame.close()
                }
            })
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Servers");
            ui.separator();

            ui.horizontal(|ui| {
                for (_index, server) in servers.iter() {
                    ui.selectable_value(
                        current_server,
                        server.index,
                        format!("Server {}", server.name),
                    );
                }

                if ui.button("+").clicked() {
                    *last_index += 1;
                    let name = "dummy";
                    servers.insert(*last_index, Server::new(name.to_string(), *last_index));
                }
            });

            if let Some(server) = servers.get_mut(current_server) {
                ui.vertical(|ui| {
                    ui.horizontal(|ui| {
                        ui.label("Name: ");
                        ui.add(TextEdit::singleline(&mut server.name).desired_width(100.0));
                    });

                    ui.horizontal(|ui| {
                        ui.label("Adresse: ");
                        ui.add(TextEdit::singleline(&mut server.addr).password(false));
                    });

                    ui.horizontal(|ui| {
                        ui.label("Tenant: ");
                        ui.add(TextEdit::singleline(&mut server.tenant).password(false));
                    });

                    ui.horizontal(|ui| {
                        ui.label("Token: ");
                        ui.add(TextEdit::singleline(&mut server.token).password(true));
                    });
                });

                if ui.button("Delete Server Tab").clicked() {
                    *to_delete = Some(*current_server);
                }

                ui.heading("Topics");
                ui.separator();

                ui.horizontal(|ui| {
                    for (_index, topic) in server.topics.iter() {
                        if ui
                            .selectable_value(
                                &mut server.current_topic,
                                topic.index,
                                format!("Topic {}", topic.name),
                            )
                            .clicked()
                        {
                            server.current_topic = topic.index;
                        }
                    }
                    if ui.button("+").clicked() {
                        server.last_index += 1;
                        let name = "dummy";
                        server
                            .topics
                            .insert(server.last_index, Topic::new(name, server.last_index));
                    }
                });

                ui.separator();

                if ui.button("Delete Topic Tab").clicked() {
                    server.topics.remove(&server.current_topic);
                }

                ui.spacing();

                let topic = server.topics.get_mut(&server.current_topic);
                if let Some(topic) = topic {
                    let pulsar_config = PulsarConfig {
                        topic: topic.name.clone(),
                        token: server.token.clone(),
                        addr: server.addr.to_string(),
                        tenant: server.tenant.to_string(),
                    };

                    ui.horizontal(|ui| {
                        ui.label("Topic: ");
                        ui.text_edit_singleline(&mut topic.name);
                    });

                    ui.horizontal(|ui| {
                        ui.vertical(|ui| {
                            ui.text_edit_multiline(&mut topic.send_message);
                            if ui.button("Send Message").clicked() {
                                send_command(Command::Send(MessageSend {
                                    data: topic.send_message.to_string(),
                                    config: pulsar_config.clone(),
                                }));
                            }
                        });

                        ui.vertical(|ui| {
                            ui.text_edit_multiline(&mut topic.receive_message);

                            ui.horizontal(|ui| {
                                ui.label("Receive");

                                if toggle::toggle_ui(ui, &mut topic.receive_data).clicked() {
                                    let destination = ConsumerKey {
                                        server_id: *current_server,
                                        topic_id: server.current_topic,
                                        app_id: *app_id,
                                    };

                                    let command = match topic.receive_data {
                                        true => Command::StartReceive {
                                            destination,
                                            config: pulsar_config,
                                        },
                                        false => Command::StopReceive(destination),
                                    };

                                    info!(?command);

                                    send_command(command);
                                }
                            });
                        });
                    });
                }
            }
        });
    }

    // Save state in case of shutdown
    fn save(&mut self, storage: &mut dyn Storage) {
        eframe::set_value(storage, APP_KEY, self)
    }
}
