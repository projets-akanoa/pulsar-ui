use pulsar_data::{ConsumerKey, PulsarConfig};

pub mod pulsar_data;
pub mod ui_data;

/// Command Send to Pulsar thread
#[derive(Clone, Debug)]
pub enum Command {
    /// Start the consumption of a topic
    StartReceive {
        config: PulsarConfig,
        destination: ConsumerKey,
    },
    /// Stop consumption and kill the consumer
    StopReceive(ConsumerKey),
    /// Send a message to a topic
    Send(MessageSend),
    /// Kill the Pulsar thread
    Kill,
}

/// The message container sends to Pulsar thread
/// that will be itself sends to topic
#[derive(Clone, Debug)]
pub struct MessageSend {
    pub(crate) data: String,
    pub(crate) config: PulsarConfig,
}

/// An event coming from Pulsar thread
#[derive(Debug, Clone)]
pub struct Event {
    pub(crate) data: String,
    pub(crate) destination: ConsumerKey,
}
