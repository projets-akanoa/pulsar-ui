use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// Represent an occurence of Pulsar server
/// keeps track of the of this server in UI
/// This one is serializable because the state is
/// restored after App restart
#[derive(Deserialize, Serialize)]
pub struct Server {
    pub(crate) index: usize,
    pub(crate) name: String,
    pub(crate) token: String,
    pub(crate) addr: String,
    pub(crate) tenant: String,
    pub(crate) topics: HashMap<usize, Topic>,
    pub(crate) current_topic: usize,
    pub(crate) last_index: usize,
}

impl Server {
    pub(crate) fn new(name: String, index: usize) -> Self {
        Server {
            index,
            name,
            token: "".to_string(),
            addr: "".to_string(),
            tenant: "".to_string(),
            topics: Default::default(),
            current_topic: 0,
            last_index: 0,
        }
    }
}

/// The UI state of a Topic section
#[derive(Default, Deserialize, Serialize, PartialEq, Clone)]
pub struct Topic {
    pub(crate) index: usize,
    pub(crate) name: String,
    #[serde(skip)]
    pub(crate) send_message: String,
    #[serde(skip)]
    pub(crate) receive_message: String,
    #[serde(skip)]
    pub(crate) receive_data: bool,
}

impl Topic {
    pub(crate) fn new(name: &str, index: usize) -> Self {
        Topic {
            name: name.to_string(),
            send_message: "".to_string(),
            receive_message: "".to_string(),
            index,
            receive_data: false,
        }
    }
}
