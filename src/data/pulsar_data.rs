use std::collections::hash_map::DefaultHasher;
use std::fmt::{Display, Formatter};
use std::hash::{Hash, Hasher};

/// Represents a Pulsar configuration on a unique topic
/// the `token` could be either a JWT or a Biscuit
#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct PulsarConfig {
    pub(crate) topic: String,
    pub(crate) token: String,
    pub(crate) addr: String,
    pub(crate) tenant: String,
}

impl PulsarConfig {
    /// Get the hash value of a particular topic definition
    pub(crate) fn to_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::default();
        self.hash(&mut hasher);
        hasher.finish()
    }
}

/// The representation of topic consumer key
#[derive(Clone, Hash, PartialEq, Eq, Debug)]
pub struct ConsumerKey {
    pub(crate) server_id: usize,
    pub(crate) topic_id: usize,
    pub(crate) app_id: u8,
}

impl Display for ConsumerKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}-{}-{}", self.app_id, self.server_id, self.topic_id)
    }
}

impl ConsumerKey {
    pub(crate) fn to_consumer_id(&self) -> u64 {
        ((self.app_id as usize) * 100000000 + self.server_id * 10000 + self.topic_id) as u64
    }
}
