//! Define a Cache with a fixed number of entries
use crate::data::pulsar_data::PulsarConfig;
use crate::pulsar_adapter::producer::PulsarSender;
use pulsar::{Producer, TokioExecutor};
use rand::seq::SliceRandom;
use std::collections::HashMap;
use thiserror::Error;
use tracing::debug;

const CACHE_CAPACITY: usize = 10;

#[derive(Error, Debug)]
pub enum CacheError {
    #[error("Pulsar error while creating Producer {0}")]
    Pulsar(pulsar::Error),
    #[error("Can't found producer in cache after insertion")]
    NotFoundOrCreated,
    #[error("Cache has a zero size capacity")]
    ZeroSized,
}

/// Create a Producer cache which evinces Least Recently Used Connections
pub struct Cache {
    producers: HashMap<u64, Producer<TokioExecutor>>,
    matrix: plru::DynamicCache,
}

impl Cache {
    pub fn new() -> Cache {
        Cache {
            producers: HashMap::default(),
            matrix: plru::create(CACHE_CAPACITY),
        }
    }

    /// Remap a u64 value to a 1 -> matrix size usize value
    /// This is done because PLRU cache doesn't do itself the
    /// remapping
    fn remap(&self, n: u64) -> usize {
        let n = n as f64;

        let b1 = 1.;
        let b2 = self.matrix.len() as f64;

        let a1 = 0.;
        let a2 = u64::MAX as f64;

        let r = b1 + (n - a1) * (b2 - b1) / (a2 - a1);

        r as usize
    }

    /// Workflow :
    /// 1. we hash the [PulsarConfig] to an integer as cache key
    /// 2. we get the PLRU cache key
    /// 3. we ask the cache Producer  map if the cache key exists
    /// 4. If so we return the Producer
    /// 5. If not
    /// 6. we create a [Producer]
    /// 7. We check whether the cache capacity is exceeded
    /// 8. If so
    /// 9. We collect all cold entries using the PLRU algorithm
    /// and chose a random one to evince
    /// 10. We remove the chosen cold entry in the [Producer] cache map
    /// 11. We insert the producer with the cache key as identifier
    /// 12. We refresh the PLRU with the PLRU cache key
    /// 13. We return the mutable reference to the Producer
    pub async fn get_or_insert(
        &mut self,
        config: &PulsarConfig,
    ) -> Result<&mut Producer<TokioExecutor>, CacheError> {
        let hash = config.to_hash();
        let remapped_hash = self.remap(hash);
        let producer = if self.producers.contains_key(&hash) {
            debug!("Producer found in cache return it");
            self.producers
                .get_mut(&hash)
                .ok_or(CacheError::NotFoundOrCreated)?
        } else {
            debug!("Producer not found in cache create it");
            let producer = PulsarSender::get_producer(config)
                .await
                .map_err(CacheError::Pulsar)?;

            // Entry must be evinced
            if self.producers.len() + 1 >= CACHE_CAPACITY {
                debug!("Cache exceeded evicting LRU entry");
                let cold_entries = self
                    .producers
                    .iter()
                    .filter_map(|(k, _)| {
                        let entry_hash = self.remap(*k);
                        (self.matrix.is_hot(entry_hash)).then_some(*k)
                    })
                    .collect::<Vec<u64>>();
                let cold_entry = cold_entries
                    .choose(&mut rand::thread_rng())
                    .ok_or(CacheError::ZeroSized)?;
                self.producers.remove_entry(cold_entry);
            }

            self.producers.entry(hash).or_insert(producer)
        };
        self.matrix.touch(remapped_hash);
        Ok(producer)
    }
}
