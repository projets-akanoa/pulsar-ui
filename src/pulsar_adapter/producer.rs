use crate::data::pulsar_data::PulsarConfig;
use pulsar::{producer, proto, Error, Producer, Pulsar, TokioExecutor};
use tracing::{debug, info};

pub struct PulsarSender {}

impl PulsarSender {
    pub(crate) async fn get_producer(
        config: &PulsarConfig,
    ) -> Result<Producer<TokioExecutor>, Error> {
        info!(?config, "Creating new producer");

        let mut builder = Pulsar::builder(&config.addr, TokioExecutor);
        let authentication = pulsar::Authentication {
            name: "token".to_string(),
            data: config.token.to_string().into_bytes(),
        };
        builder = builder.with_auth(authentication);

        let client = builder.build().await?;

        let topic = format!("persistent://{}/{}", config.tenant, config.topic);

        let producer = client
            .producer()
            .with_topic(topic)
            .with_options(producer::ProducerOptions {
                schema: Some(proto::Schema {
                    r#type: proto::schema::Type::String as i32,
                    ..Default::default()
                }),
                ..Default::default()
            })
            .build()
            .await?;

        Ok(producer)
    }

    pub(crate) async fn send(
        producer: &mut Producer<TokioExecutor>,
        message: &str,
    ) -> Result<(), Error> {
        debug!(message =? message, "Sending message");
        producer.send(message).await?.await.map(|_| ())
    }
}
