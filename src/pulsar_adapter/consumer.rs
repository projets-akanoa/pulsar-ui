use crate::data::pulsar_data::ConsumerKey;
use crate::data::pulsar_data::PulsarConfig;
use pulsar::consumer::InitialPosition;
use pulsar::{Consumer, ConsumerOptions, Error, Pulsar, SubType, TokioExecutor};
use tracing::info;

pub struct PulsarReceiver {}

impl PulsarReceiver {
    /// Create a Pulsar [Consumer](Consumer)
    /// Set the subscription name using the `destination` as key.
    ///
    /// Also uses the `consumer_key` to defined a unique Consumer ID.
    pub(crate) async fn get_consumer(
        config: &PulsarConfig,
        consumer_key: &ConsumerKey,
    ) -> Result<Consumer<String, TokioExecutor>, Error> {
        let mut builder = Pulsar::builder(config.addr.to_string(), TokioExecutor);
        let authentication = pulsar::Authentication {
            name: "token".to_string(),
            data: config.token.to_string().into_bytes(),
        };
        builder = builder.with_auth(authentication);

        let client = builder.build().await?;

        info!("Client created");

        let topic = format!("persistent://{}/{}", config.tenant, config.topic);

        let subscription_name = consumer_key.to_string();

        let consumer = client
            .consumer()
            .with_topic(topic)
            .with_subscription_type(SubType::Shared)
            .with_options(ConsumerOptions {
                initial_position: InitialPosition::Latest,
                ..Default::default()
            })
            .with_subscription(subscription_name.clone())
            .with_consumer_name(subscription_name)
            .with_consumer_id(consumer_key.to_consumer_id())
            .build::<String>()
            .await?;

        info!("Consumer created");

        Ok(consumer)
    }
}
