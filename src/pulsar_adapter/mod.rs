use crate::data::pulsar_data::ConsumerKey;
use crate::data::{Command, Event};
use crate::pulsar_adapter::cache::Cache;
use crate::{CHANNEL_COMMAND, CHANNEL_EVENT, CHANNEL_KILL};
use consumer::PulsarReceiver;
use futures::TryStreamExt;
use producer::PulsarSender;
use pulsar::{Consumer, TokioExecutor};
use std::collections::HashMap;
use tokio::join;
use tokio::sync::mpsc::Receiver;
use tracing::{debug, error, info, warn};

mod cache;
mod consumer;
mod producer;

pub struct PulsarAdapter {
    producer_cache: Cache,
}

/// Receiving job
///
/// Infinite loop on Pulsar message receiving, the consumed message is head to a the [CHANNEL_EVENT](static@CHANNEL_EVENT)  using
/// the `destination`  parameter as key.
///
/// Dies if the `drop_signal` is triggered.
async fn receive(
    mut consumer: Consumer<String, TokioExecutor>,
    destination: ConsumerKey,
    mut drop_signal: Receiver<()>,
) -> Consumer<String, TokioExecutor> {
    loop {
        tokio::select! {
            _ = drop_signal.recv() => {
                match consumer.close().await {
                    Ok(_) => info!(sub=consumer.subscription(), "Closing consumer"),
                    Err(err) => warn!(?err, consumer=?destination, "Unable to close consumer")
                };

                break
            },
            message = consumer.try_next() => {
                if let Ok(Some(m)) = message {
                    debug!(details=?m.message_id, "Message");
                    if let Err(err) = consumer.ack(&m).await {
                        warn!(?err, consumer=%destination, "Unable to acknowledge message")
                    }

                    match m.deserialize() {
                        Ok(data) => {
                            let event = Event {
                        destination: destination.clone(),
                        data,
                    };
                            let send_result = CHANNEL_EVENT.0.send(event.clone());
                    if let Err(err) = send_result {
                        warn!(?err, consumer=%destination, event=?event, "Unable to send  message to event channel")
                    }
                        },
                        Err(err) => {
                            let raw_message = String::from_utf8_lossy(&m.payload.data);
                            warn!(?err, consumer=%destination, %raw_message, "Unable to send  message to event channel")
                        }
                    }
                }
            }
        }
    }
    info!(?destination, "Closing consumer job");
    consumer
}

impl Default for PulsarAdapter {
    fn default() -> Self {
        PulsarAdapter {
            producer_cache: Cache::new(),
        }
    }
}

impl PulsarAdapter {
    pub fn new() -> Self {
        Default::default()
    }

    pub(crate) async fn run(&mut self) {
        let mut consumers = HashMap::new();

        while let Ok(command) = CHANNEL_COMMAND.1.recv() {
            match command {
                Command::StartReceive {
                    config,
                    destination: consumer_key,
                } => {
                    debug!("Receiver created");

                    match PulsarReceiver::get_consumer(&config, &consumer_key).await {
                        Ok(consumer) => {
                            let message_destination = consumer_key.clone();
                            let (drop_signal_trigger, drop_signal) = tokio::sync::mpsc::channel(1);
                            let receive_job_handle = tokio::task::spawn(async move {
                                receive(consumer, message_destination, drop_signal).await
                            });

                            debug!(?consumer_key, "Inserting consumer to receiving list");

                            consumers
                                .insert(consumer_key, (receive_job_handle, drop_signal_trigger));
                        }
                        Err(err) => {
                            warn!(?err, ?consumer_key, "Unable to create consumer")
                        }
                    }
                }
                Command::StopReceive(consumer_key) => {
                    if let Some((job_handle, drop_signal_trigger)) = consumers.remove(&consumer_key)
                    {
                        debug!(?consumer_key, "Closing consumer");
                        if let Err(err) = drop_signal_trigger.send(()).await {
                            warn!(
                                ?err,
                                consumer = consumer_key.to_string(),
                                "Unable to send drop signal to consumer job"
                            );
                        }

                        let receive_job_result = join!(job_handle).0;
                        if let Err(err) = receive_job_result {
                            warn!(?err, "Consumer thread has panicked")
                        }
                    }
                }
                Command::Send(message) => {
                    debug!("send message");

                    let producer = self.producer_cache.get_or_insert(&message.config).await;

                    match producer {
                        Ok(producer) => {
                            if let Err(err) = PulsarSender::send(producer, &message.data).await {
                                error!(?err, "Unable to send message")
                            }
                        }
                        Err(err) => {
                            error!(?err, "Unable to get producer");
                        }
                    }

                    debug!("end send message");
                }
                Command::Kill => {
                    for (destination, (j, tx)) in consumers.into_iter() {
                        debug!(?destination, "Closing consumer");
                        if let Err(err) = tx.send(()).await {
                            warn!(?err, "Unable send Kill fallback message")
                        }
                        if let Err(err) = join!(j).0 {
                            warn!(?err, "Consumer thread has panicked")
                        }
                    }
                    if let Err(err) = CHANNEL_KILL.0.send(()) {
                        warn!(?err, "Unable to send kill")
                    }
                    break;
                }
            }
        }
    }
}
