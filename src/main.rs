use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use tracing_subscriber::{fmt, EnvFilter};

#[tokio::main]
async fn main() -> eframe::Result<()> {
    tracing_subscriber::registry()
        .with(fmt::layer())
        .with(EnvFilter::from_default_env())
        .init();

    let natives_options = eframe::NativeOptions::default();
    eframe::run_native(
        "pulsar UI",
        natives_options,
        Box::new(|cc| Box::new(pulsar_ui::PulsarApp::new(cc))),
    )
}
